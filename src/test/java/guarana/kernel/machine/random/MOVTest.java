package guarana.kernel.machine.random;

import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Random;
import guarana.kernel.memory.Read;
import guarana.kernel.machine.Instruction;
import guarana.kernel.register.BX;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MOVTest {

    @Test
    @DisplayName("deve registar em AX valores")
    void deveRegistrarEmAxValores() {

        final BX bx = new BX();

        final Read<RegisterRandom<Integer>> registers = key -> bx;

        final Instruction mov = new MOV
                (
                        new Memory<>() {
                            @Override
                            public String register() {
                                return "bx";
                            }

                            @Override
                            public Literal<Integer> value() {
                                return () -> 2;
                            }
                        },
                        registers,
                        new Random() {
                            @Override
                            public String get(int index) {
                                return "0";
                            }

                            @Override
                            public void insert(String t) {

                            }
                        }

                );

        mov.execute();

        Assertions.assertEquals(0, bx.toLiteral().get());

    }
}