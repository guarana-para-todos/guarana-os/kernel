package guarana.kernel.machine.random;

import guarana.kernel.machine.Instruction;
import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Random;
import guarana.kernel.register.LX;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CMPTest {

    @Test
    @DisplayName("deve registar em LX comparação")
    void deveRegistrarEmAxValores() {

        final LX lx = new LX();

        final Instruction instruction = new CMP
                (
                        new Memory<>() {
                            @Override
                            public Literal<Integer> register() {
                                return () -> 0x1;
                            }

                            @Override
                            public Literal<Integer> value() {
                                return () -> 1;
                            }
                        },
                        lx,
                        new Random() {
                            @Override
                            public String get(int index) {
                                return "1";
                            }

                            @Override
                            public void insert(String t) {

                            }
                        }
                );


        instruction.execute();

        Assertions.assertEquals(1, lx.toLiteral().get());

    }
}