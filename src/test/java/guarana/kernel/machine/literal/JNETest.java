package guarana.kernel.machine.literal;

import guarana.kernel.register.LB;
import guarana.kernel.register.LX;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class JNETest {

    @Test
    @DisplayName("Deve inserir label no registro LB")
    void deveInserirLabelNoRegistroLB() {

        final LX lx = new LX();

        lx.insert(() -> 0);

        final LB lb = new LB();

        final JNE jne = new JNE(lx, lb, () -> "B");

        jne.execute();

        Assertions.assertEquals("B", lb.toLiteral().get());

    }

    @Test
    @DisplayName("quando lx = 1 , não deve inserir label no registro LB")
    void quandoLxIgual1NaoDeveInserirLabelNoRegistroLB() {

        final LX lx = new LX();

        lx.insert(() -> 1);

        final LB lb = new LB();

        final JNE jne = new JNE(lx, lb, () -> "B");

        jne.execute();

        Assertions.assertNull(lb.toLiteral().get());

    }
}