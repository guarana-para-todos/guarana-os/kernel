package guarana.kernel.machine.literal;

import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Read;
import guarana.kernel.machine.Instruction;
import guarana.kernel.register.AX;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MOVTest {

    @Test
    @DisplayName("deve registar em AX valores")
    void deveRegistrarEmAxValores() {

        final AX ax = new AX();

        final Read<RegisterLiteral<Integer>> registers = key -> ax;

        final Instruction mov = new MOV
                (
                        new Memory<>() {
                            @Override
                            public String register() {
                                return "ax";
                            }

                            @Override
                            public Literal<Integer> value() {
                                return () -> 2;
                            }
                        },
                        registers
                );

        mov.execute();

        Assertions.assertEquals(2, ax.toLiteral().get());

    }
}