package guarana.kernel.machine.data;

import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Read;
import guarana.kernel.machine.Instruction;
import guarana.kernel.register.CX;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class MOVTest {

    @Test
    @DisplayName("deve registar em CX valores")
    void deveRegistrarEmCxValores() {

        final Data statics = new Data();

        statics.insert("v12_driver", "123");

        final RegisterData cx = new CX();

        final Read<RegisterData> registers = key -> cx;

        final Instruction mov = new ADD
                (
                        new Memory<>() {
                            @Override
                            public String register() {
                                return "cx";
                            }

                            @Override
                            public String value() {
                                return "v12_driver";
                            }
                        },
                        statics,
                        registers
                );

        mov.execute();

        final Map<String, String> map = cx.toMap();

        Assertions.assertEquals("driver", map.keySet().stream().findFirst().orElse("nothing"));

        Assertions.assertEquals("123", map.get("driver"));

    }

    private static final class Data implements Read<String> {

        public Data() {
            this.map = new HashMap<>();
        }

        private final Map<String, String> map;

        @Override
        public String value(final String key) {
            return map.get(key);
        }

        public void insert(final String key, final String value) {
            map.put(key, value);
        }

    }
}