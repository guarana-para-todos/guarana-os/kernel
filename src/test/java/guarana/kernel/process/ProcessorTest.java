package guarana.kernel.process;

import guarana.kernel.machine.Instruction;
import guarana.kernel.machine.data.RegisterData;
import guarana.kernel.machine.literal.JNE;
import guarana.kernel.machine.literal.RegisterLiteral;
import guarana.kernel.machine.random.CMP;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Random;
import guarana.kernel.memory.Read;
import guarana.kernel.register.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

class ProcessorTest {

    @Test
    @DisplayName("Deve executar linhas de instruções")
    void deveExecutarLinhasDeInstrucoes() {

        record MemoryForTest<A, B>(A register, B value) implements Memory<A, B> {
        }

        final Map<String, AX> registersMap = Map.of("ax", new AX(), "bx", new BX(), "lx", new LX());

        final Read<RegisterLiteral<Integer>> registersLiterals = registersMap::get;

        final RegisterData cx = new CX();

        final Read<RegisterData> registersDatas = key -> cx;

        final LB lb = new LB();

        final List<Instruction> instrucoes = List.of
                (
                        new guarana.kernel.machine.literal.MOV(new MemoryForTest<>("ax", () -> 0x2), registersLiterals),
                        new guarana.kernel.machine.literal.MOV(new MemoryForTest<>("bx", () -> 0x1), registersLiterals),
                        new guarana.kernel.machine.data.MOV
                                (
                                        new MemoryForTest<>("cx", "v0_data"),
                                        key -> Map.of("v0_data", "123").get(key),
                                        registersDatas
                                ),
                        new CMP
                                (
                                        new MemoryForTest<>(() -> 0x1, () -> 1),

                                        registersLiterals.value("lx"),

                                        new Random() {

                                            @Override
                                            public String get(int index) {
                                                return List.of("0", "0").get(--index);
                                            }

                                            @Override
                                            public void insert(final String t) {

                                            }

                                        }
                                ),

                        new JNE(registersLiterals.value("lx"), lb, () -> "A"),

                        new guarana.kernel.machine.literal.MOV(new MemoryForTest<>("ax", () -> 0x3), registersLiterals)
                );

        final RegisterLiteral<String> bl = new BL();

        final Read<RegisterLiteral<String>> registersControl = key -> Map.of("lb", lb, "bl", bl).get(key);

        final Processor processor = new Processor(instrucoes, registersControl);

        processor.insertLabel("A", 6);

        final Status executar = processor.executar();

        Assertions.assertEquals(Status.FINALIZADO, executar);

        Assertions.assertEquals(0x3, registersLiterals.value("ax").toLiteral().get());

        Assertions.assertEquals(0x1, registersLiterals.value("bx").toLiteral().get());

        final Map<String, String> map = cx.toMap();

        Assertions.assertEquals("data", map.keySet().stream().findFirst().orElseThrow());

        Assertions.assertEquals("123", map.get("data"));

        Assertions.assertEquals(0x1, registersLiterals.value("lx").toLiteral().get());

    }

}