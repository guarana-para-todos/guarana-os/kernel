grammar machine;

main
    : data* text
    ;

data
    : 'section' '.data' variable*
    ;

text
    : 'section' '.text' instruction+ ret
    ;

instruction
    : unary
    | binary
    | label
    ;

unary
    : OPERATOR memory
    ;

binary
    : OPERATOR register ',' (memory|register)
    ;

label
    : IDENTIFIER ':' instruction
    ;

ret
    : IDENTIFIER ':' RET
    | RET
    ;

RET
    : 'ret'
    ;

register
    : REGISTER
    | literal
    ;

memory
    : static
    | random
    | literal
    ;

random
    : '[' 'esp' '+' index ']'
    ;

static
    : IDENTIFIER
    ;

REGISTER
    : 'cx'
    | 'ax'
    | 'bx'
    | 'esp'
    | 'esx'
    ;

OPERATOR
    : 'add'
    | 'mov'
    | 'call'
    | 'jne'
    | 'cmp'
    | 'and'
    ;

literal
    : HEXADECIMAL
    ;

HEXADECIMAL
    : [0][x][0-9]+
    ;

index
    : NUMERIC
    ;

variable
    : IDENTIFIER 'db' VALUE
    ;

NUMERIC
    : [0-9]+
    ;

IDENTIFIER
    : [_a-zA-Z][a-zA-Z0-9_]*
    ;

VALUE
   : '"' DOUBLE_QUOTE_CHAR* '"'
   ;

fragment DOUBLE_QUOTE_CHAR
   : ~["\\\r\n]
   | ESCAPE_SEQUENCE
   ;

fragment ESCAPE_SEQUENCE
   : '\\'
   ( NEWLINE
   | UNICODE_SEQUENCE       // \u1234
   | ['"\\/bfnrtv]          // single escape char
   | ~['"\\bfnrtv0-9xu\r\n] // non escape char
   | '0'                    // \0
   | 'x' HEX HEX            // \x3a
   )
   ;

fragment UNICODE_SEQUENCE
   : 'u' HEX HEX HEX HEX
   ;

fragment HEX
   : [0-9a-fA-F]
   ;

fragment NEWLINE
   : '\r\n'
   | [\r\n\u2028\u2029]
   ;

WS
   : [ \r\n\t]+ -> skip
   ;

Comment
  :  ';' ~( '\r' | '\n' )* -> skip
  ;
