package guarana.kernel.process;

import guarana.kernel.machine.Instruction;
import guarana.kernel.machine.literal.RegisterLiteral;
import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Read;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public final class Processor {

    public Processor(final List<Instruction> instructions, final Read<RegisterLiteral<String>> registersControl) {
        this.instructions = instructions;
        this.registers = registersControl;
        this.programCounter = 0;
        limit = instructions.size();
        labels = new HashMap<>();
    }

    private final Read<RegisterLiteral<String>> registers;

    private final List<Instruction> instructions;

    private int programCounter;

    private final int limit;

    private final Map<Literal<String>, Integer> labels;

    public void insertLabel(final String label, final int instructionCounter) {
        labels.put(() -> label, instructionCounter - 1);
    }

    public Status executar() {

        while (programCounter < limit) {

            final Instruction instruction = instructions.get(programCounter);

            instruction.execute();

            // VERIFICANDO SE PRECISO DAR UM SALTO ATÉ LABEL

            final RegisterLiteral<String> lb = registers.value("lb");

            final Literal<String> literal = lb.toLiteral();

            if (literal.get() != null) {
                programCounter = labels.get(literal);
                lb.erase();
            } else programCounter++;

            // FIM

            final Literal<String> bl = registers.value("bl").toLiteral();

            if (Objects.equals(bl.get(), "1")) {
                return Status.BLOQUEADO;
            }

        }

        return Status.FINALIZADO;

    }

}
