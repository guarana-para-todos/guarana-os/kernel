package guarana.kernel.process;

public enum Status {

    PRONTO,
    EXECUTANDO,
    BLOQUEADO,
    FINALIZADO

}
