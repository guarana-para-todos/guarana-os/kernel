package guarana.kernel.memory;

public interface Label {

    String value();

}
