package guarana.kernel.memory;

public interface Random {

    String get(final int index);

    void insert(final String t);

}
