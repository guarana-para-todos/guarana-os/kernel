package guarana.kernel.memory;

public interface Literal<T> {

    T get();

}
