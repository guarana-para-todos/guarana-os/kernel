package guarana.kernel.memory;

public interface Memory<A, B> {

    A register();

    B value();

}
