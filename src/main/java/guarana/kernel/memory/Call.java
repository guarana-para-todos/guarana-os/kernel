package guarana.kernel.memory;

import guarana.kernel.machine.data.RegisterData;

public interface Call {

    void execute(final Read<RegisterData> register);

}
