package guarana.kernel.memory;

public interface Read<T> {

    T value(final String key);

}
