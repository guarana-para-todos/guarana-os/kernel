package guarana.kernel.register;

import guarana.kernel.machine.literal.RegisterLiteral;
import guarana.kernel.memory.Literal;

/**
 * Registro representando bloqueio de processo
 * <p>
 * Se Atual == true, processo bloqueado
 * Se Atual == false, processo não bloqueado
 */
public final class BL implements RegisterLiteral<String> {

    public BL() {
        atual = () -> false;
    }

    private Literal<Boolean> atual;

    @Override
    public void insert(final Literal<String> literal) {
        atual = () -> Boolean.valueOf(literal.get());
    }

    @Override
    public Literal<String> toLiteral() {
        return () -> atual.get().toString();
    }

    @Override
    public void erase() {
        atual = () -> false;
    }

}
