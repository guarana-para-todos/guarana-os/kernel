package guarana.kernel.register;

import guarana.kernel.machine.random.RegisterRandom;
import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Random;

public class AX implements RegisterRandom<Integer> {

    public AX() {
        atual = () -> 0;
    }

    private Literal<Integer> atual;

    @Override
    public void insert(final Literal<Integer> literal) {
        atual = literal;
    }

    @Override
    public void insert(final Random memory, final Literal<Integer> literal) {
        final String s = memory.get(literal.get());
        atual = () -> Integer.parseInt(s);
    }

    @Override
    public Literal<Integer> toLiteral() {
        return () -> atual.get();
    }

    @Override
    public void erase() {
        atual = () -> 0;
    }

}
