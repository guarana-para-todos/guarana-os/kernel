package guarana.kernel.register;

import guarana.kernel.machine.literal.RegisterLiteral;
import guarana.kernel.memory.Literal;

public final class LB implements RegisterLiteral<String> {

    public LB() {
        atual = () -> null;
    }

    private Literal<String> atual;

    @Override
    public void insert(final Literal<String> literal) {
        atual = literal;
    }

    @Override
    public Literal<String> toLiteral() {
        return () -> atual.get();
    }

    @Override
    public void erase() {
        atual = null;
    }

}
