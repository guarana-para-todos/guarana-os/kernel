package guarana.kernel.register;

import guarana.kernel.memory.Read;
import guarana.kernel.machine.data.RegisterData;

import java.util.HashMap;
import java.util.Map;

public final class CX implements RegisterData {

    public CX() {
        this.map = new HashMap<>();
    }

    private final Map<String, String> map;

    @Override
    public void insert(final Read<String> memory, final String key) {

        final String regex = "v[0-9]+_";

        final String newKey = key.replaceAll(regex, "");

        final String value = memory.value(key);

        map.put(newKey, value);

    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(map);
    }

}
