package guarana.kernel.machine.data;

import guarana.kernel.memory.Read;

import java.util.Map;

public interface RegisterData {

    void insert(final Read<String> memory, final String key);

    Map<String, String> toMap();

}
