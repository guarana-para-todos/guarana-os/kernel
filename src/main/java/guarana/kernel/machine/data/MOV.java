package guarana.kernel.machine.data;

import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Read;
import guarana.kernel.machine.Instruction;

public class MOV implements Instruction {

    public MOV(final Memory<String,String> memory, final Read<String> statics, final Read<RegisterData> registers) {
        this.statics = statics;
        this.registers = registers;
        this.memory = memory;
    }

    private final Memory<String,String> memory;

    private final Read<String> statics;

    private final Read<RegisterData> registers;

    @Override
    public void execute() {
        registers.value(memory.register()).insert(statics, memory.value());
    }

}
