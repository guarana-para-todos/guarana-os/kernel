package guarana.kernel.machine.data;

import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Read;

public final class ADD extends MOV {

    public ADD(final Memory<String,String> memory, final Read<String> statics, final Read<RegisterData> registers) {
        super(memory, statics, registers);
    }

}
