package guarana.kernel.machine.literal;

import guarana.kernel.memory.Literal;

public interface RegisterLiteral<T> {

    void insert(final Literal<T> literal);

    Literal<T> toLiteral();

    void erase();

}
