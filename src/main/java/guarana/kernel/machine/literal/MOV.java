package guarana.kernel.machine.literal;

import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Read;
import guarana.kernel.machine.Instruction;

public final class MOV implements Instruction {

    public MOV(final Memory<String, Literal<Integer>> memory, final Read<RegisterLiteral<Integer>> registers) {
        this.memory = memory;
        this.registers = registers;
    }

    private final Memory<String,Literal<Integer>> memory;

    private final Read<RegisterLiteral<Integer>> registers;

    @Override
    public void execute() {
        registers.value(memory.register()).insert(memory.value());
    }

}
