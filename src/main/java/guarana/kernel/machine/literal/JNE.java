package guarana.kernel.machine.literal;

import guarana.kernel.machine.Instruction;
import guarana.kernel.memory.Literal;

public final class JNE implements Instruction {

    public JNE(final RegisterLiteral<Integer> lx, final RegisterLiteral<String> lb, final Literal<String> value) {
        this.lx = lx;
        this.lb = lb;
        this.value = value;
    }

    private final RegisterLiteral<Integer> lx;

    private final RegisterLiteral<String> lb;

    private final Literal<String> value;

    @Override
    public void execute() {

        final Integer result = lx.toLiteral().get();

        if (result == 0) lb.insert(value);

    }

}
