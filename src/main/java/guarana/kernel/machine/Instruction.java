package guarana.kernel.machine;

public interface Instruction {

    void execute();

}
