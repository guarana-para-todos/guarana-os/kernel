package guarana.kernel.machine.random;

import guarana.kernel.machine.literal.RegisterLiteral;
import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Random;

public interface RegisterRandom<T> extends RegisterLiteral<T> {

    void insert(final Random memory, final Literal<T> literal);

}
