package guarana.kernel.machine.random;

import guarana.kernel.machine.Instruction;
import guarana.kernel.machine.literal.RegisterLiteral;
import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Random;

public final class CMP implements Instruction {

    public CMP(final Memory<Literal<Integer>, Literal<Integer>> memory, final RegisterLiteral<Integer> register, final Random random) {
        this.memory = memory;
        this.register = register;
        this.random = random;
    }

    private final Random random;

    private final Memory<Literal<Integer>, Literal<Integer>> memory;

    private final RegisterLiteral<Integer> register;

    @Override
    public void execute() {

        final Literal<Integer> lt = memory.register();

        final String s = random.get(memory.value().get());

        final int value = Integer.parseInt(s);

        register.insert(() -> value == lt.get() ? 1 : 0);

    }

}
