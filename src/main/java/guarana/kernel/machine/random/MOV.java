package guarana.kernel.machine.random;

import guarana.kernel.memory.Literal;
import guarana.kernel.memory.Memory;
import guarana.kernel.memory.Random;
import guarana.kernel.memory.Read;
import guarana.kernel.machine.Instruction;

public final class MOV implements Instruction {

    public MOV(final Memory<String, Literal<Integer>> memory, final Read<RegisterRandom<Integer>> registers, final Random random) {
        this.memory = memory;
        this.registers = registers;
        this.random = random;
    }

    private final Random random;

    private final Memory<String,Literal<Integer>> memory;

    private final Read<RegisterRandom<Integer>> registers;

    @Override
    public void execute() {
        registers.value(memory.register()).insert(random, memory.value());
    }

}
